Sequelize = require('sequelize'),
  
  connection = new Sequelize('postgres', 'larichev', 'darker', {
    host: 'localhost',
    dialect: 'postgres',
    logging: false,
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
  });

module.exports = connection;