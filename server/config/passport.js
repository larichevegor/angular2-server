const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/user');
// var configAuth = require('./auth')
var passport = require('passport')

var GoogleTokenStrategy = require('passport-google-token').Strategy;
var TwitterTokenStrategy = require('passport-twitter-token');
var FacebookTokenStrategy = require('passport-facebook-token');




let Users = connection.models.users;




module.exports = function (passport) {
  let opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
  opts.secretOrKey = '321321';

  passport.use(new JwtStrategy(opts, (jwt_payload, done) => {

    User.getUserById(jwt_payload.data.id, (err, user) => {
      if (err) {
        return done(err, false);
      }
      if (user) {
        return done(null, user);
      } else {
        return done(null, false);
      }
    });
  }));



  // Google ////////////////////////////////////////////////////
  passport.use(new GoogleTokenStrategy({
    clientID: '252761645479-rqncud8uaf7mf4d7c9rqc1860d4f4n0e.apps.googleusercontent.com',
    clientSecret: 'jlAnru7K_ViGIsiXPHw5jH4e'
  },
    function (accessToken, refreshToken, profile, done) {
      console.log(1111, accessToken, profile)
      Users.findAll({
        where:
        {
          auth_id: profile.id
        }
      }).then(user => {
        console.log('aaaa@@@@', user.length)

        if (user.length != 0) {
          console.log('user was found', user.id)
          return done(null, user);
        }

        user = new Users({
          auth_id: profile.id,
          username: profile.displayName,
          email: profile._json.email,
          avatar: profile._json.picture,
          token: accessToken
        });

        user.save(function (err) {
          console.log("saving user ...", user);
          return done(null, user);
        });
      })
        .catch()//
    }
  ));

  // Twitter /////////////////////////////////////////
  passport.use(new TwitterTokenStrategy({
    consumerKey: 'TWITTER_CONSUMER_KEY',
    consumerSecret: 'TWITTER_CONSUMER_SECRET'
  },
    function (accessToken, refreshToken, profile, done) {
      console.log(1111, accessToken, profile)
      Users.findAll({
        where:
        {
          auth_id: profile.id
        }
      }).then(user => {
        console.log('aaaa@@@@', user.length)

        if (user.length != 0) {
          console.log('user was found', user.id)
          return done(null, user);
        }

        user = new Users({
          auth_id: profile.id,
          username: profile.displayName,
          email: profile._json.email,
          avatar: profile._json.picture,
          token: accessToken
        });

        user.save(function (err) {
          console.log("saving user ...", user);
          return done(null, user);
        });
      })
        .catch()//
    }
  ));

  // Facebook /////////////////////////////////////////
  passport.use(new FacebookTokenStrategy({
    clientID: 'FACEBOOK_APP_ID',
    clientSecret: 'FACEBOOK_APP_SECRET'
  }, function (accessToken, refreshToken, profile, done) {
    console.log(1111, accessToken, profile)
    Users.findAll({
      where:
      {
        auth_id: profile.id
      }
    }).then(user => {
      console.log('aaaa@@@@', user.length)

      if (user.length != 0) {
        console.log('user was found', user.id)
        return done(null, user);
      }

      user = new Users({
        auth_id: profile.id,
        username: profile.displayName,
        email: profile._json.email,
        avatar: profile._json.picture,
        token: accessToken
      });

      user.save(function (err) {
        console.log("saving user ...", user);
        return done(null, user);
      });
    })
      .catch()//
  }
  ));
}