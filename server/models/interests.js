connection = require('../config/connectdb');


let Interests;
module.exports = function (sequelize, DataTypes) {
  var interests = sequelize.define('interests',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: DataTypes.TEXT,
      },
    });


  interests.associate = function (models) {
    interests.belongsToMany(models.users, {
      through: {
        model: 'userinteresrel',
        unique: false
      },
      foreignKey: 'interes_id',
      constraints: false
    });
    
    interests.belongsToMany(models.polls, {
      through: {
        model: 'pollinteresrel',
        unique: false
      },
      foreignKey: 'interes_id',
      constraints: false
    });
  }

  return interests;
}