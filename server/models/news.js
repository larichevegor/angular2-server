connection = require('../config/connectdb');
jwt = require('jsonwebtoken');
var jwtDecode = require('jwt-decode');


connection.import('./newstagsrel');
let newstagsrel = connection.models.newstagsrel;



let NEWS;
module.exports = function (sequelize, DataTypes) {
  NEWS = News = sequelize.define('news',
    {
      authorname:
      {
        type: DataTypes.TEXT,
      },
      newsTitle:
      {
        type: DataTypes.TEXT,

      },
      newsText:
      {
        type: DataTypes.TEXT,

      },
      authorId:
      {
        type: DataTypes.TEXT,
      },
      myDate:
      {
        type: Sequelize.DATE(6),
        defaultValue: Sequelize.NOW,
        get: function () {
          // 'this' allows you to access attributes of the instance
          if (this.getDataValue('myDate')) {
            return this.getDataValue('myDate').toISOString().slice(0, 19);
          }
        },
      },
      avatar:
      {
        type: DataTypes.TEXT,
      },

    })

  return News;
}



module.exports.addNews = function (newNews, callback) {
  return new Promise((resolve, reject) => {
    resolve(newNews.save());
  });
}

module.exports.getAllNews = function (callback) {
  NEWS
    .findAll()
    .then(dbRes => callback(null, dbRes))
    .catch(err => callback(err));
}


// module.exports.getNewsForUser = function (id, callback) {
//   console.log(id);
//   const query = {
//     where: {
//       authorId: id
//     }
//   }
//   console.log(query);




//     .catch(err => callback(err));

//   NEWS
//     .findAll(query)
//     .then(dbRes => callback(null, dbRes))
//     .catch(err => callback(err));
// }



module.exports.deleteNews = function (token, idOfNews, callback) {
  var decoded = jwt.decode(token, { complete: true });
  let id = decoded.payload.data.id.toString();
  const query = {
    where: {
      id: idOfNews,
      authorId: id,
    }
  }
  console.log(query);

  return new Promise((resolve, reject) => {
    resolve(NEWS.destroy(query));
  });
}

// module.exports.changeNews = function (idOfNews, id, body, callback) {
//   var decoded = jwt.decode(body.token, { complete: true });
//   let id = decoded.payload.data.id.toString();

//   const query = {
//     where: {
//       id: idOfNews,
//       authorId: id,
//     }
//   }


//   new Promise((resolve, reject) => {
//     resolve(
//       NEWS.find(query).then(function (news) {
//         if (news) {
//           news.updateAttributes({
//             newsText: body.newsText,
//             newsTitle: body.newsTitle
//           })
//         }
//       })
//     );
//   });

//   return NEWS
//     .findAll(query)
//     .then(dbRes => callback(null, dbRes))
//     .catch(err => callback(err));



// }





