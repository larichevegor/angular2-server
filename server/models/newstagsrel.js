connection = require('../config/connectdb');
let News = connection.models.news;
connection.import('./tags');
let Tags = connection.models.tags;

let NEWSTAGSREL;
module.exports = function (sequelize, DataTypes) {
  NEWSTAGSREL = NewsTagsRel = sequelize.define('newstagsrel',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      tag_id: {
        type: DataTypes.INTEGER,
        unique: 'newstagsrel_taggable'
      },
      taggable: {
        type: DataTypes.STRING,
        unique: 'newstagsrel_taggable'
      },
      taggable_id: {
        type: DataTypes.INTEGER,
        unique: 'newstagsrel_taggable',
        references: null
      }

    })

  return NewsTagsRel; 
}



module.exports.addNewsTagsRel = function (NewsTagsRel, callback) {
  return new Promise((resolve, reject) => {
    resolve(NewsTagsRel.save());
  });
}
