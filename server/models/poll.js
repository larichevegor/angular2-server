connection = require('../config/connectdb');


module.exports = function (sequelize, DataTypes) {
    var Polls = sequelize.define('polls',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            pollText:
            {
                type: DataTypes.TEXT,
            },
            id_owner: {
                type: DataTypes.INTEGER,
            },
            availableForAll:
            {
                type: DataTypes.BOOLEAN,

            },
        });

    Polls.associate = function (models) {
        Polls.hasMany(models.voteitems, { as: 'cards' });
        Polls.belongsToMany(models.interests, {
            through: {
                model: 'pollinteresrel',
                unique: false,
                scope: {
                    interesable: 'polls'
                }
            },
            foreignKey: 'interesable_id',
            constraints: false
        });
    }

    return Polls;
}

