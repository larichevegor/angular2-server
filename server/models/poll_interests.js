connection = require('../config/connectdb');


let POLLINTERESREL;
module.exports = function (sequelize, DataTypes) {
  POLLINTERESREL = PollInteresRel = sequelize.define('pollinteresrel',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      interes_id: {
        type: DataTypes.INTEGER,
        unique: 'pollinteresrel_interesable'
      },
      interesable: {
        type: DataTypes.STRING,
        unique: 'pollinteresrel_interesable'
      },
      interesable_id: {
        type: DataTypes.INTEGER,
        unique: 'pollinteresrel_interesable',
        references: null
      }

    })
return PollInteresRel; 
}