let bcrypt = require('bcryptjs');
connection = require('../config/connectdb');


module.exports = function (sequelize, DataTypes) {
  var Users = sequelize.define('users',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      username:
      {
        type: DataTypes.TEXT,
      },
      email: {
        type: DataTypes.TEXT,
      },
      avatar:
      {
        type: DataTypes.TEXT,

      },
      auth_id:
      {
        type: DataTypes.TEXT,

      },
      token:
      {
        type: DataTypes.TEXT,

      }
    });

  Users.associate = function (models) {
    Users.belongsToMany(models.interests, {
      through: {
        model: 'userinteresrel',
        unique: false,
        scope: {
          interesable: 'users'
        }
      },
      foreignKey: 'interesable_id',
      constraints: false
    });
  }

  return Users;
}




module.exports.getUserById = function (id, callback) {
  const query = {
    where: {
      id: id
    }
  }
  User
    .findOne(query)
    .then(dbRes => callback(null, dbRes))
    .catch(err => callback(err));
}

module.exports.getUserByEmail = function (email, callback) {
  const query = {
    where: {
      email: email
    }
  }
  User
    .findOne(query)
    .then(dbRes => callback(null, dbRes))
    .catch(err => callback(err));
}

module.exports.addUser = function (newUser, callback) {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) return reject(err);
        newUser.password = hash;
        resolve(newUser.save());
      });
    });
  });
}

module.exports.comparePassword = function (candidatePassword, hash, callback) {
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if (err) throw err;
    callback(null, isMatch);
  });
}

module.exports.uniqueCheck = function (email, callback) {
  const query = {
    where: {
      email: email
    }
  }
  console.log(query);
  return new Promise((resolve, reject) => {
    resolve(User.findOne(query));
  });

}
module.exports.getProfileForUser = function (id, callback) {
  console.log(id);
  const query = {
    where: {
      id: id
    }
  }
  console.log(query);

  User
    .findAll(query)
    .then(dbRes => callback(null, dbRes))
    .catch(err => callback(err));
}

module.exports.changeProfileUser = function (body, callback) {
  var decoded = jwt.decode(body.token, { complete: true });
  let id = decoded.payload.data.id.toString();

  const query = {
    where: {
      id: id
    }
  }

  return new Promise((resolve, reject) => {
    resolve(
      User.find(query).then(function (user) {
        if (user) {
          user.updateAttributes({
            username: body.username,
            dateOfBirth: body.dateOfBirth,
            sex: body.sex,
            avatar: body.avatar
          })
        }
      })
    );
  });
}