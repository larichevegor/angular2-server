connection = require('../config/connectdb');


let VOTE;
module.exports = function (sequelize, DataTypes) {
    VOTE = Vote = sequelize.define('vote',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            id_poll: {
                type: DataTypes.INTEGER,
            },
            id_owner: {
                type: DataTypes.INTEGER,
            },
            id_result:
            {
                type: DataTypes.INTEGER,

            },  
        })

    return Vote;
}