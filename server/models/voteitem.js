connection = require('../config/connectdb');


module.exports = function (sequelize, DataTypes) {
    var VoteItems = sequelize.define('voteitems',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            description: {
                type: DataTypes.STRING,
            },
            image: {
                type: DataTypes.STRING,
            },
            id_poll:
            {
                type: DataTypes.INTEGER,

            },
            votecount:
            {
                type: DataTypes.INTEGER,

            },
        })

    return VoteItems;
}

