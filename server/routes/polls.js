var express = require('express'),
    router = express.Router(),
    path = require('path'),
    Sequelize = require('sequelize'),
    passport = require('passport'),
    connection = require('../config/connectdb'),
    NEWS = require('../models/news'),
    jwt = require('jsonwebtoken'),
    multer = require('multer'),
    upload = multer({ dest: './public/images/' }),
    TAGS = require('../models/tags'),
    NEWSTAGSREL = require('../models/newstagsrel'),
    models = require('../models');

// let Interests = connection.models.interests;
// let pollinteresrel = connection.models.pollinteresrel;
// let Polls = connection.models.polls;
// let userinteresrel = connection.models.userinteresrel;
// let Vote = connection.models.vote;
// let Voteitems = connection.models.voteitems;

console.log(models.interests.prototype)

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'imgs/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
})

router.get('/', (req, res, next) => {
    models.polls.findAll({
        include: [{
            model: models.interests,
        }]
    }).then(dbRes => {
        // find voiteitems items where is id of poll = id of this poll
        console.log(dbRes);
    })
});

router.post('/', upload.array('photo', 12), (req, res, next) => {
    console.log(req.files)
    let filesPath = [];
    let imageDescription = [];
    let decoded = jwt.decode(token, { complete: true });
    let id = decoded.payload.data.id.toString();
    let name = decoded.payload.data.username.toString();
    let description = JSON.parse(req.body.data).description
    let interests = JSON.parse(req.body.interests)
    let token = JSON.parse(req.body.auth).token

    for (i = 0; i < req.files.length; i++) {
        path = 'http://localhost:1337' + req.files[i].path.substring(6, req.files[i].path.length)
        filesPath.push(path);
        imageDescription.push(JSON.parse(req.body.data).votesCard[i]);

    }
    console.log(filesPath, imageDescription, token, description)
    console.log(imageDescription[0].imageDescription)

    console.log(id, name)

    let newPoll = new models.polls({
        authorname: name,
        pollText: description,
        id_owner: id,
        availableForAll: false

    })

    newPoll.save()
        .then(dbRes => {
            console.log(dbRes.dataValues.id)
            models.interests.findAll({ where: { id: interests } }).then((interests) => {
                console.log(interests)
                dbRes.addInterest(interests, { interesable: 'polls' }).then(() => {
                    let newVoteitems = [];
                    for (let i = 0; i < filesPath.length; i++) {
                        newVoteitems[i] = new Voteitems({
                            description: imageDescription[i].imageDescription,
                            image: filesPath[i],
                            id_poll: dbRes.dataValues.id,
                            // Позже связал карточки с голосованием (hasMany), чтобы появился метод в прототипе addCards
                            // не надо будет теперь вручную айди привязывать
                            votecount: 0,
                        })
                        newVoteitems[i].save().then(dbRes => {
                            console.log(dbRes)
                        });

                    }
                    res.json({ success: true, msg: 'Poll added successful' });
                })
            })
        })
        .catch(err => {
            // console.log('###err', err);
            res.json({ success: false, msg: 'Failed to add news' });
        });

});

router.post('/vote', (req, res, next) => {
    // search vote where is id of vote = this id of voite ---- count ++, find polls with this id,
    // and send response
});

router.get('/all', (req, res, next) => {
    var tasksPerPage = Number(req.header('X-Page-Size'));
    var currentPage = req.header('X-Current-Page');
    let offSet = currentPage * tasksPerPage - tasksPerPage;

    models.polls.findAll({
        offset: offSet,
        limit: tasksPerPage,
        include: [{
            model: models.interests,
        },
        {
            model: models.voteitems,
            as: 'cards'
        }]
    }).then(dbRes => {
        return res.json(dbRes)
    })
});

module.exports = router;

