var express = require('express'),
    router = express.Router(),
    path = require('path'),
    Sequelize = require('sequelize'),
    passport = require('passport'),
    connection = require('../config/connectdb'),
    User = require('../models/user'),
    jwt = require('jsonwebtoken'),
    multer = require('multer'),
    upload = multer({ dest: 'public/images/' }),
    // connection.import('../models/user');
    // let Users = connection.models.users;
    // let Interests = connection.models.interests;
    models = require('../models');


let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'imgs/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
})

router.put('/setimg', upload.single('avatar'), function (req, res, next) {
    console.log('sadasdasdasdasdasd', req.body);
    let avatarpath = 'http://localhost:1337' + req.file.path.substring(6, req.file.path.length)
    var decoded = jwt.decode(req.headers.token, { complete: true });
    let id = decoded.payload.data.id.toString();
    const query = {
        where: {
            id: id
        }
    }

    models.users.find(query).then(function (user) {
        if (user) {
            user.updateAttributes({
                avatar: avatarpath
            })
        }
    })
})





// Register
router.post('/register', (req, res, next) => {

    let newUser = new models.users({
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        dateOfBirth: req.body.dateOfBirth,
        sex: req.body.sex

    });
    console.log(newUser.email);
    let check = User.uniqueCheck(newUser.email);
    console.log(check)

    check
        .then(dbRes => {
            if (dbRes == null) {
                console.log(dbRes);
                let savedUser = User.addUser(newUser);
                savedUser
                    .then(dbRes => {
                        console.log('###dbRes', dbRes);
                        res.json({ success: true, msg: 'User registered successful' });
                    })
                    .catch(err => {
                        console.log('###err', err);
                        res.json({ success: false, msg: 'Failed to register user' });
                    });
            }
            else {
                console.log('###dbRes', dbRes);
                res.json({ success: false, msg: 'User already exist' });
            }
        })
        .catch(err => {
            console.log('###err', err);
            res.json({ success: false, msg: 'Failed to register user' });
        });



});



// Profile
router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    console.log('12313');
    res.json({ user: req.user });
});

router.get('/profile/:id', (req, res, next) => {
    let id = req.params.id;
    User.getProfileForUser(id, (err, user) => {
        if (err) throw err;
        if (user) {
            return res.json(user);
        }
    });
});

router.get('/interests/', (req, res, next) => {
    model.interests.findAll().then(data => {
        res.json(data)
    })
});



router.put('/', (req, res, next) => {
    let body = req.body
    let updatedUser = User.changeProfileUser(body);

    updatedUser
        .then(dbRes => {
            console.log('###dbRes', dbRes);
            res.json({ success: true, msg: 'User profile update successful' });
        })
        .catch(err => {
            console.log('###err', err);
            res.json({ success: false, msg: 'Error' });
        });
});





module.exports = router;