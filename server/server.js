var express = require('express'),
  path = require('path'),
  bodyParser = require('body-parser'),
  port = 1337,
  pg = require('pg'),
  Sequelize = require('sequelize'),
  connection = require('./config/connectdb'),
  _ = require('./models'),
  news = require('./routes/news'),
  users = require('./routes/users'),
  polls = require('./routes/polls'),
  auth = require('./routes/auth'),
  cors = require('cors'),
  passport = require('passport'),
  expressSession = require('express-session');

app = express();
app.use(cors());
//view engine
app.set('views', path.join(__dirname, './client/src'));
app.set('view engine', 'ejs');

app.engine('html', require('ejs').renderFile);

// Set static folder
app.use(express.static(path.join(__dirname, './public')));

//Body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(expressSession({ secret: '123321' }));

app.use(passport.initialize());

require('./config/passport')(passport);

app.use('/users', users);
app.use('/news', news);
app.use('/auth', auth);
app.use('/polls', polls);





connection.sync().then(function () {
  app.listen(port, function () {
    console.log("Server Started on PORT 1337");
  });
});

